# Modern device access methods


An incomplete document containing information about the available options to make a modern way for applications to access devices. Contributions welcome. 

## Raw device access via `/dev`

Drawbacks:
- Can't be reasonably filtered
- Potentially (well, closer to definitely) insecure
- May need specific udev rules installed (e.g. steam-devices)

When it comes to input devices with `/dev`, it's a complicated problem. You can somewhat filter them, like `--device=dri`, but it can quickly get to be a hell of connections and device hotplugs, and the sandbox can't reasonably mount them all in at runtime. So you have a *very* limited set of usable filters from the start.

But right now, this is how it's done. So long as the appropriate udev rules are installed (like steam-devices), and you have full access to `/dev`, you can read the devices. This isn't *ideal*, for a few reasons, the main of which I've listed at the beginning of this section. 

The main problem with this is the sandboxing aspect; it's nonexistent. So a no-go for a modern way to access raw devices from the start.

## A portal

Portals are traditionally sandbox-friendly, and solve most of the major issues:
- Can be managed dynamically (at runtime)
- Are sandbox-friendly
- Can be filtered

There are a few issues with using it for input devices though:
- It's not typical for applications to need to *ask* for access to input devices, especially gamepads
- Input handling is normally done in the display server and friends, not an external service
- An exception to the former is remote desktop, but even that goes through something related to the display server at some point (e.g. libei, which goes straight to the compositor)

Global keybinds also fit in a bit weirdly here; right now they're asked for and triggered via dbus, though it's the compositor's job to tell the portal when a keybind is active. Possibly relevant, [add inputcapture trigger for shortcuts and immediate capture](https://github.com/flatpak/xdg-desktop-portal/pull/1035). Probably not a worry, as portals also handle screen capture. 

The main problem I see with using a portal is that the compositor can't control it. 
- It can't restrict the devices that a certain surface gets 
- It can't control when surfaces get access to the devices
- It's not really suitable for the potential use cases of Wayland. While it's fine on a desktop, it is in no way suitable for a game console like the Steam Deck, where games aren't expected to have access to devices when they're not in use. 

So it's usually best done for generic USB devices in [USB portal](https://github.com/flatpak/xdg-desktop-portal/pull/559), which would be useful for e.g. Yubikeys or device firmware upgrades, but not gamepads or raw access to the keyboard and mouse.

It's also worth noting that anything not integrated with Wayland would inherit many of the same problems a portal would have.

## A generic Wayland protocol

The [inputfd_v1 protocol](https://gitlab.freedesktop.org/wayland/wayland-protocols/-/blob/a01218ad0e64e05b183362080929aba8ea1eac01/staging/inputfd/inputfd-v1.xml) was brought up in the past, and is probably the best out of all of the possible solutions. 
- It works from a sandbox
- The compositor can control it
- Can be managed dynamically

This protocol works in three steps (simplified):
- A client gets a seat (collection of devices) via `get_seat_evdev`
- `get_seat_evdev` returns a collection of `wp_inputfd_device_evdev_v1`s:
> The wp_inputfd_device_evdev interface represents one device node in the Linux kernel evdev interface. The fd passed to the client supports the ioctls and read/write commands of that interface. The protocol makes no guarantees which ioctls are available on the fd, this decision is made by the kernel.
- From here, the client gets `removed`, `focus_out`, and `focus_in` events, with the latter two always coming in pairs - a `removed` event will always be sent after a `focus_out` event. 

This functions more in line with what game consoles do; games don't get any input events when they don't have focus. What this *doesn't* do, however, is work how the desktop space currently does. Right now, apparently it's standard for games to *always* have access to gamepad events, and only lose keyboard and mouse events. This, of course, complicates things a bit. 

How do we solve this problem? Well, we could make that compositor policy. Then the Steam Deck gets to revoke input events, a desktop system could provide (a possibly filtered version of) input events to non-focused clients, and it could even be user customizable. 

It also probably wouldn't be very Wayland-like (nor feasible) to have the compositor filter the devices so clients *always* get input events, at least for gamepads. So that's probably not going to be worth figuring out. It sounds like it's within the spec though, if compositors were to want to go that path.

This protocol is also *very* generic - it doesn't have anything to do with device types or who should use it. So it having special behavior for a specific set of devices probably wouldn't make sense. 

## A gamepad Wayland protocol

We could also have a dedicated protocol for gamepads, but this isn't viable for one main reason: controllers are too diverse. You just can't easily put them into a Wayland protocol. You'd either end up too generic or too specific, and things would move *fast*.  
